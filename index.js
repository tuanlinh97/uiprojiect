/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import LoginSplash from './src/component/LoginSplash';
import Login from './src/component/Login';
import Navigate from './src/navigation/Navigate';
import MenuComponent from './src/component/MenuComponent';
import ListAccComponent from './src/component/ListAccComponent';
import ItemVach from './src/control/ItemVach';
import ListAccData from './src/network/ListAccData';
import TransactionDetails from './src/component/TransactionDetails';
import CardDetails from './src/component/CardDetails';
import Tabbar from './src/navigation/Tabbar';
import Transaction from './src/component/Transaction';


AppRegistry.registerComponent(appName, () => Tabbar);
