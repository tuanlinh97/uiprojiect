import React, { Component } from 'react';
import { View ,StyleSheet,Image} from 'react-native';

import CardDetails from '../component/CardDetails';
import Transaction from '../component/Transaction';
import { Router, Scene } from 'react-native-router-flux';
import {Container,Header,Content,Tab,Tabs} from 'native-base';

const Tabbar = () => {
    return (
        <View style={{flexDirection:'column',flex:1}}>
        <View style={styles.headercontainer}>
        <Image
            source={require('../image/logoMenu.png')}
            style={{ width: 127, height: 56, marginBottom: 15 }}

        />
      </View>
      
            {/* <Router>
          
        <Scene
          key="root"
          tabs
          tabBarStyle={{ backgroundColor: '#FFFFFF' }}
          labelStyle={{ fontSize: 16, marginBottom: 15 }}
          hideNavBar
          
        >
          
          <Scene key="ichi" tabBarLabel="CARD DETAILS">
            <Scene
              key="card"
              component={CardDetails}
              hideNavBar
            />
            
          </Scene>
          
          <Scene key="ni" tabBarLabel="TRANSACTION">
            <Scene
              key="trans"
              component={Transaction}
              hideNavBar
              
            />
            
          </Scene>
         
        </Scene>
      </Router> */}
      
        <View style={{flexDirection:'column',flex:1}}>
        <Tabs 
            tabBarUnderlineStyle={{backgroundColor:'#004558'}}
            style={{marginLeft:30,marginTop:30,marginRight:30}}
            
        >
          <Tab heading="CARD DETAILS" activeTextStyle={{color:'#004558'}}>
            <CardDetails />
          </Tab>
          <Tab heading="TRANSACTION" activeTextStyle={{color:'#004558'}}>
            <Transaction />
          </Tab>
          
        </Tabs>
        </View>
      
      </View>
      
      
      
    );
  }
  const styles = StyleSheet.create({
    headercontainer: {
        backgroundColor: '#E32D44',
        left: 0,
        right: 0,
        top: 0,
        height: '16%',
        justifyContent: 'flex-end',
        alignItems: 'center'

    }
  })
  
  export default Tabbar;