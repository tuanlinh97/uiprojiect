import React, { Component } from 'react';
import {} from 'react-native';
import Login from '../component/Login';
import MenuComponent from '../component/MenuComponent';
import {Scene , Actions,Router} from 'react-native-router-flux';
import Menu2 from '../component/Menu2';
import Transaction from '../component/Transaction';

export default class Navigate extends Component {
    render() {
        return(
            // dieu huong
      <Router>
      <Scene key="root"
        hideNavBar
      >
        <Scene
          key="login"
          component={Login}
          initial
        />

        <Scene
          key="menu"
          component={MenuComponent}
          title="menu"
        />

        <Scene 
          key = 'trans'
          component = {Transaction}
          
        />
      </Scene>
    </Router>
        );
    }
}