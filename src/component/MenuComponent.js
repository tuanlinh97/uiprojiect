import React, { Component } from 'react';
import { View, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { Text } from 'react-native-elements';

export default class MenuComponent extends Component {

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }} >
                <View style={styles.headercontainer}>
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <View style={{ justifyContent: 'flex-end', alignItems: 'flex-start', flex: 1, paddingBottom: 30, paddingLeft: 20 }}>
                            <Image
                                source={require('../image/menutop.png')}
                            />
                        </View>
                        <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 1, paddingBottom: 15, paddingRight: 100 }}>
                            <Image
                                source={require('../image/logoMenu.png')}
                                style={{ width: 127, height: 56 }}

                            />
                        </View>
                        <TouchableOpacity>
                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', paddingBottom: 24, paddingRight: 20 }}>
                                <Image
                                    source={require('../image/searchLogo.png')}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{
                    backgroundColor: '#004558'
                    , height: 47,
                    width: '100%',
                    justifyContent: 'center',
                    paddingLeft: 30
                }}>
                    <Text style={{
                        fontSize: 18,
                        color: '#FFFFFF'

                    }}>
                        ALLIED PICKFORDS-MELBOURNE
                    </Text>
                </View>
                <View style={{ height: 608, width: '100%', backgroundColor: '#F5F5F5', flexDirection: 'column', flex: 1 }}>
                    <TouchableOpacity>
                    <View style={styles.itemView}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image
                                source={require('../image/weightbalance.png')}
                                style={{ width: 16, height: 16, marginTop: 19, marginLeft: 15 }}
                            />
                            <Text style={{ fontSize: 16, marginTop: 15, marginLeft: 10, color: '#4A4A4A' }}>
                                Current Balance
                        </Text>
                            <View style={{
                                flex: 1,
                                marginLeft: 100,
                                marginRight: 20,
                                marginTop: 15,
                                height: 23,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderWidth: 0.5,
                                borderColor: '#E32D44'
                            }}>

                                <Text style={{ color: '#E32D44', fontSize: 14 }}>TOP UP</Text>

                            </View>


                        </View>
                        <View style={{
                            marginTop: 30,
                            alignItems: 'flex-end',
                            marginRight: 15

                        }}>
                            <Text
                                style={{
                                    fontSize: 40,
                                    color: '#4A4A4A'

                                }}
                            >$-1,242.94</Text>
                        </View>


                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                    <View style={styles.itemView2}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image
                                source={require('../image/money1.png')}
                                style={{ width: 16, height: 16, marginTop: 19, marginLeft: 15 }}
                            />
                            <Text style={{ fontSize: 16, marginTop: 15, marginLeft: 10, color: '#4A4A4A' }}>
                                Last Billed Amount
                        </Text>



                        </View>
                        <View style={{
                            marginTop: 22,
                            alignItems: 'flex-end',
                            marginRight: 15,
                            marginBottom:10

                        }}>
                            <Text
                                style={{
                                    fontSize: 40,
                                    color: '#4A4A4A'

                                }}
                            >$666.79</Text>
                        </View>
                        <View style={{
                            borderWidth: 1,
                            borderColor: '#ECECEC'
                        }}>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'flex-end',alignItems:'flex-end',marginTop:15}}>
                            <Text
                                style = {{fontSize:14,color:'#A4A4A4',marginRight:3}}
                            >Last Billed Amount</Text>
                            <Text style={{color:'#4A4A4A',marginRight:10}}>30 April 2011</Text>
                        </View>
                        

                    </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                    <View style={styles.itemView3}>
                    <View style={{ flexDirection: 'row' }}>
                            <Image
                                source={require('../image/badge.png')}
                                style={{ width: 16, height: 16, marginTop: 19, marginLeft: 15 }}
                            />
                            <Text style={{ fontSize: 16, marginTop: 15, marginLeft: 10, color: '#4A4A4A' }}>
                             Last Payment Amount
                        </Text>



                        </View>
                        <View style={{
                            marginTop: 22,
                            alignItems: 'flex-end',
                            marginRight: 15,
                            marginBottom:10

                        }}>
                            <Text
                                style={{
                                    fontSize: 40,
                                    color: '#4A4A4A'

                                }}
                            >$666.79</Text>
                        </View>
                        <View style={{
                            borderWidth: 1,
                            borderColor: '#ECECEC'
                        }}>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'flex-end',alignItems:'flex-end',marginTop:15}}>
                            <Text
                                style = {{fontSize:14,color:'#A4A4A4',marginRight:3}}
                            >Last Billed Amount</Text>
                            <Text style={{color:'#4A4A4A',marginRight:10}}>30 April 2011</Text>
                        </View>
                    </View>
                    </TouchableOpacity>
                </View>
                <View style={{ backgroundColor: '#004558', height: '5%', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={styles.textbottom}>All rights reserved, CALTEX ©2017</Text>
                </View>
            </View>

        );
    }
}

const styles = StyleSheet.create({
    headercontainer: {
        backgroundColor: '#E32D44',
        left: 0,
        right: 0,
        top: 0,
        height: '16%',
        flexDirection: 'row'
    },
    itemView: {
        flexDirection: 'column',
        marginTop: 20,
        marginLeft: 18,
        height: 121,
        width: '92%',
        borderWidth: 0.5,
        backgroundColor: '#FFFFFF',
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 1
        },
        borderRadius: 9,
        borderColor: '#FFFFFF'
    },
    itemView2: {
        width: '92%',
        height: 159,
        borderWidth: 0.5,
        marginTop: 20,
        marginLeft: 18,
        backgroundColor: '#FFFFFF',
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 1
        },
        borderRadius: 8,
        borderColor: '#FFFFFF'

    },
    itemView3: {
        width: '92%',
        height: 159,
        borderWidth: 0.5,
        marginTop: 20,
        marginLeft: 18,
        backgroundColor: '#FFFFFF',
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 1
        },
        borderRadius: 8,
        borderColor: '#FFFFFF'
    },
    textbottom: {
        fontSize: 12,
        color: '#FFFFFF'
    }
})