import React, { Component } from 'react';
import {
    View,
    ImageBackground,
    Text,
    TouchableOpacity,
    Image,
    SafeAreaView,
    TextInput,
    StyleSheet,
    KeyboardAvoidingView
}
    from 'react-native';

import LoginSplash from '../component/LoginSplash';
import { CheckBox } from 'react-native-elements';
import Navigate from '../navigation/Navigate';
import {Actions} from 'react-native-router-flux';

// performTimeConsumingTask = async() => {
//     return new Promise((resolve) =>
//       setTimeout(
//         () => { resolve('result') },
//         2000
//       )
//     );
//   }
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //     // gia lap du lieu dang load
            //     isLoading : true
            // }
            email: '',
            password: '',
            checked: ''

        }

    }

    render() {
        // if(this.state.isLoading) {
        //     return <LoginSplash />
        // }
        return (

            <ImageBackground
                source={require('../image/bgLogin.png')}
                style={{ width: '100%', height: '100%' }}
            >

              <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
                    <View style={styles.logocontainer}>

                        <Image
                            source={require('../image/logocaltex.png')}
                            style={{ width: 232, height: 173 }}
                        />

                    </View>
                    <View style={styles.inputContainer}>
                        <TextInput
                            style={styles.txtinput}
                            placeholder=" Login ID"
                            onChangeText={(email) => this.setState({ email })}
                            value={this.state.email}
                            returnKeyType='next'
                            onSubmitEditing={()  => this.passwordInput.focus()}
                        />
                        <TextInput
                            style={styles.txtPass}
                            placeholder="Password"
                            onChangeText={(password) => this.setState({ password })}
                            value={this.state.password}
                            secureTextEntry={true}
                            returnKeyType='go'
                            ref={(input) => this.passwordInput = input}

                        />
                        <TouchableOpacity onPress = {() => Actions.menu()}>
                            <View style={styles.btnLogin}>
                                <Text style={styles.textLogin}>Log In</Text>
                            </View>
                        </TouchableOpacity>
                        <CheckBox 
                            onPress={() => { this.setState({ checked: !this.state.checked }) }}
                            checked = {this.state.checked}
                            center
                            checkedIcon={<Image source={require('../image/icontich.png')} />}
                            uncheckedIcon={<Image source={require('../image/ic_rect_uncheck.png')} />}
                            title = 'Remember me?' 
                            containerStyle = {{backgroundColor : 'transparent',borderWidth:0}}
                            textStyle={{color:'white'}}
                            
                            
                        />



                    </View>

                    <View style={styles.bot}>
                        <Text style={{ color: '#B5B5B5' }}>Terms & Conditions  |  Privacy Policies</Text>
                    </View>


                </KeyboardAvoidingView>


            </ImageBackground>
        );
    }
    // async componentDidMount() {
    //     // Preload data from an external API
    //     // Preload data using AsyncStorage
    //     const data = await this.performTimeConsumingTask();
    //     if (data !== null) {
    //       this.setState({ isLoading: false });
    //     }
    //   }
}
const styles = StyleSheet.create({
    // txtinput : {
    //     borderWidth : 0.5,
    //     height:50,

    // }
    container: {
        flex: 1,

    },
    logocontainer: {

        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 250
    },

    inputContainer: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 250,
        height: 200,

    },
    txtinput: {
        borderWidth: 0.5,
        height: 50,
        width: '95%',
        marginLeft: 10,
        borderRadius: 40,
        backgroundColor: 'white',
        paddingLeft: '4%'

    },
    txtPass: {
        borderWidth: 0.5,
        height: 50,
        width: '95%',
        marginTop: 10,
        marginLeft: 10,
        borderRadius: 40,
        backgroundColor: 'white',
        paddingLeft: '4%'
    },
    btnLogin: {
        borderWidth: 0.5,
        height: 50,
        backgroundColor: '#E92539',
        borderRadius: 40,
        marginTop: 10,
        marginLeft: 10,
        width: '95%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    textLogin: {
        color: '#F7F7F7',
    },
    bot: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        height: 70,
        alignItems: 'center'
    }
});



