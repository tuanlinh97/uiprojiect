import React, { Component } from 'react';
import { View, Text, SectionList, StyleSheet, TextInput,Image, TouchableOpacity } from 'react-native';
import {Actions} from 'react-native-router-flux';


export default class Transaction extends Component {
    render() {

        return (
            <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'column', flex: 1 }}>

                </View>
                
                <View style={{ flex: 4 , flexDirection:'column'}}>
                    
                    <View style={styles.txtitem2}>
                        <View style={{
                            width: '4%',
                            height: 60,
                            backgroundColor: "#004558",


                        }}>

                        </View>
                        <View style={{ flexDirection: 'column' }}>
                            <Text style={styles.txtItemview2}>CALTEX YANCHEP</Text>
                            <Text style={{ marginLeft: 18, fontSize: 14, color: '#E32D44' }}>$178.5</Text>
                            
                        </View>
                        <TouchableOpacity onPress = {() => Actions.trans()}>
                        <Image 
                                    source = {require('../image/muiten.png')}
                                    style={{justifyContent:'center',alignSelf:'flex-end',marginLeft:120,marginTop:22}}
                                    
                                />
                                </TouchableOpacity>
                    </View>
                </View>
                
            </View>

        );
    }
}

const styles = StyleSheet.create({
    txtItemview2: {
        marginLeft: 20,
        color: '#004558',
        fontSize: 16,
        marginTop: 17
    },
    item2: { marginLeft: 100, fontSize: 16, marginTop: 17, color: '#004558' },
    textbottom: {

        fontSize: 12,
        color: '#FFFFFF'
    },
    txtitem2: {
        backgroundColor: 'white',
        width: '95%',
        height: 60,
        borderWidth: 0.5,
        marginTop: 15,
        marginLeft: 10,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        borderColor: '#DDD9D9',
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 3
        },
        flexDirection: 'row'
    },
});