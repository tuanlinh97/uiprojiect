import React, { Component } from 'react';
import { View, StyleSheet, Image, Text, ScrollView } from 'react-native';

export default class TransactionDetails extends Component {
    render() {
        return (
            <View>
                
                    <View style={{ height: '95%', flexDirection: 'column', backgroundColor: '#F5F5F5' }}>
                        <View style={styles.headercontainer}>
                            <Image
                                source={require('../image/logoMenu.png')}
                                style={{ width: 127, height: 56, marginBottom: 15 }}

                            />
                        </View>
                        <ScrollView>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{
                                marginTop: 30, marginLeft: 20, fontSize: 16, color: '#004558'
                            }}>
                                Approve Code
                </Text>
                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <Text style={{
                                    marginTop: 30, marginRight: 30, fontSize: 16, color: '#E32D44'
                                }}>
                                    1234567890
                </Text>

                            </View>
                        </View>
                        <View style={{ height: '95%%' }}>
                            <View style={styles.itemView1}>
                                <View style={{
                                    width: '4%',
                                    height: 182,
                                    backgroundColor: "#004558",


                                }}>

                                </View>
                                <View style={{ flexDirection: 'column' }}>
                                    <Text style={styles.txtItemview1}>Date</Text>
                                    <Text style={{ marginLeft: 180, fontSize: 16, marginTop: -20, color: '#004558' }}>2017-07-27</Text>
                                    <Text style={styles.txtItemview1}>Time</Text>
                                    <Text style={{ marginLeft: 180, fontSize: 16, marginTop: -20, color: '#004558' }}>19:08:48</Text>
                                    <Text style={styles.txtItemview1}>Station</Text>
                                    <Text style={styles.item1}>Caltec Holland</Text>
                                    <Text style={styles.txtItemview1}>Pump</Text>
                                    <Text style={styles.item1}>8</Text>
                                    <Text style={styles.txtItemview1}>Fuel Type</Text>
                                    <Text style={styles.item1}>Premium 95</Text>
                                    <Text style={styles.txtItemview1}>Volume</Text>
                                    <Text style={styles.item1}>60 litre</Text>
                                </View>

                            </View>
                            <View style={styles.txtitem2}>
                                <View style={{
                                    width: '4%',
                                    height: 50,
                                    backgroundColor: "#004558",


                                }}>

                                </View>
                                <Text style={styles.txtItemview2}>Subtotal</Text>
                                <Text style={styles.item2}>SGD 100,00.00</Text>
                            </View>
                            <View style={styles.txtitem2}>
                                <View style={{
                                    width: '4%',
                                    height: 50,
                                    backgroundColor: "#004558",


                                }}>

                                </View>
                                <Text style={styles.txtItemview2}>Discount Type</Text>
                                <Text style={{ marginLeft: 80, fontSize: 16, marginTop: 17, color: '#004558' }}>OCBC plus!</Text>
                            </View>
                            <View style={styles.txtitem2}>
                                <View style={{
                                    width: '4%',
                                    height: 50,
                                    backgroundColor: "#004558",


                                }}>

                                </View>
                                <Text style={styles.txtItemview2}>Discount </Text>
                                <Text style={{ marginLeft: 160, fontSize: 16, marginTop: 17, color: '#0AA82E' }}>-20%</Text>
                            </View>
                            <View style={styles.txtitem2}>
                                <View style={{
                                    width: '4%',
                                    height: 50,
                                    backgroundColor: "#004558",


                                }}>

                                </View>
                                <Text style={styles.txtItemview2}>Payment type</Text>
                                <Text style={{ marginLeft: 60, fontSize: 16, marginTop: 17, color: '#A4A4A4' }}>HSBC ...... 12345</Text>
                            </View>
                            <View style={styles.txtitem2}>
                                <View style={{
                                    width: '4%',
                                    height: 50,
                                    backgroundColor: "#004558",


                                }}>

                                </View>
                                <Text style={styles.txtItemview2}>TOTAL</Text>
                                <Text style={{ marginLeft: 130, fontSize: 16, marginTop: 17, color: '#E32D44' }}>SGD 80,00.00</Text>
                            </View>
                            <View style={styles.txtitem2}>
                                <View style={{
                                    width: '4%',
                                    height: 50,
                                    backgroundColor: "#E32D44",


                                }}>

                                </View>
                                <Text style={styles.txtItemview2}>LINKPOINTS COLLECTED</Text>
                                <Text style={{ marginLeft: 30, fontSize: 16, marginTop: 17, color: '#E32D44' }}>25 points</Text>
                            </View>
                        </View>
                        </ScrollView>

                    </View>

               
                <View style={{ backgroundColor: '#004558', height: '5%', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={styles.textbottom}>All rights reserved, CALTEX ©2017</Text>
                </View>

            </View>



        );
    }
}

const styles = StyleSheet.create({
    headercontainer: {
        backgroundColor: '#E32D44',
        left: 0,
        right: 0,
        top: 0,
        height: '16%',
        justifyContent: 'flex-end',
        alignItems: 'center'

    },
    itemView1: {
        backgroundColor: 'white',
        width: '90%',
        height: 182,
        borderWidth: 0.5,
        marginTop: 20,
        marginLeft: 20,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        borderColor: '#DDD9D9',
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 3
        },

        flexDirection: 'row'
    },
    txtItemview1: {
        marginLeft: 20,
        color: '#004558',
        fontSize: 16,
        marginTop: 10
    },
    item1: { marginLeft: 180, fontSize: 16, marginTop: -20, color: '#004558' },
    txtitem2: {
        backgroundColor: 'white',
        width: '90%',
        height: 50,
        borderWidth: 0.5,
        marginTop: 15,
        marginLeft: 20,
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        borderColor: '#DDD9D9',
        shadowOpacity: 0.3,
        shadowOffset: {
            width: 0,
            height: 3
        },
        flexDirection: 'row'
    },
    txtItemview2: {
        marginLeft: 20,
        color: '#004558',
        fontSize: 16,
        marginTop: 17
    },
    item2: { marginLeft: 100, fontSize: 16, marginTop: 17, color: '#004558' },
    textbottom: {

        fontSize: 12,
        color: '#FFFFFF'
    }



})