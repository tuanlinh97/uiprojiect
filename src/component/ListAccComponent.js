import React, { Component } from 'react';
import { View, StyleSheet, Image, TouchableOpacity, Text, TextInput, FlatList, TouchableHighlight } from 'react-native';
import ItemVach from '../control/ItemVach';
import { CheckBox } from 'react-native-elements';
import ListAccData from '../network/ListAccData';

class ListData extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { itemm, indexx } = this.props
        return (
            <TouchableOpacity >
                <View style={{ backgroundColor: indexx % 2 == 0 ? '#F7F4F4' : 'white' }}>
                    <Text style={styles.itemName}>
                        {itemm.name}
                    </Text>

                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ color: '#A4A4A4', marginLeft: 20, marginTop: 10 }}>Expiry Date</Text>
                        <Text style={{ marginLeft: 40, marginTop: 10, color: '#004558' }}>
                            {itemm.ExpiryDate}
                        </Text>
                        <Text style={{ marginLeft: 120, marginBottom: 20, marginTop: -1, fontSize: 18, color: '#004558' }}>{itemm.Price}</Text>
                        <Image
                            source={require('../image/muiten.png')}
                            style={{ marginLeft: '6%' }}
                        />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={{ color: '#A4A4A4', marginLeft: 20, paddingBottom: 15, marginTop: -15 }}>Card Number</Text>
                        <Text style={{ marginLeft: 29, color: '#004558', marginTop: -15 }}>
                            {itemm.CardNumber}
                        </Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

export default class ListAccComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {

            search: '',
            checked: false,
            ListAccData: ListAccData,

        }
    }
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <View style={styles.headercontainer}>
                    <View style={{ flexDirection: 'row', flex: 1 }}>
                        <View style={{ justifyContent: 'flex-end', alignItems: 'flex-start', flex: 1, paddingBottom: 30, paddingLeft: 20 }}>
                            <Image
                                source={require('../image/menutop.png')}
                            />
                        </View>
                        <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 1, paddingBottom: 15, paddingRight: 150 }}>
                            <Image
                                source={require('../image/logoMenu.png')}
                                style={{ width: 127, height: 56 }}

                            />
                        </View>
                        {/* <TouchableOpacity>
                            <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'flex-end', paddingBottom: 24, paddingRight: 20 }}>
                                <Image
                                    source={require('../image/searchLogo.png')}
                                />
                            </View>
                        </TouchableOpacity> */}
                    </View>
                </View>
                {/* <View style={{flexDirection:'row',flex:1}}>
                <View style = {styles.seachBox}>
                    <Text
                        style ={{fontSize:14,color:'#A4A4A4', marginLeft:12}}
                    >Search your card</Text>
                    
                </View>
                <View style = {{marginLeft:100,flex:1}}>
                <ItemVach />
                </View>
                
                </View> */}
                <View style={{height:50,flexDirection: 'row' }}>
                    <TextInput
                        onChangeText={(search) => this.setState({ search })}
                        placeholder='Search your Card'
                        style={{ borderWidth: 0.5, width: '50%', borderColor: '#ECECEC', borderRightColor: '#ECECEC', fontSize: 16, paddingLeft: 16 }}
                    />
                    <View style={{ borderWidth: 0.5, width: '35%', borderColor: '#ECECEC', flexDirection: 'row', borderRightColor: '#ECECEC', alignItems: 'center' }}>
                        <CheckBox
                            containerStyle = {{width:40}}
                            checkedIcon={<Image source={require('../image/icontich.png')} />}
                            uncheckedIcon={<Image source={require('../image/ic_rect_uncheck.png')} />}
                            checked={this.state.checked}
                            onPress={() => { this.setState({ checked: !this.state.checked }) }}



                        />
                        <Text style={{ marginLeft: -20, fontSize: 14, color: '#656565' }}>
                            Active Cards
                </Text>

                    </View>
                    <View style={{ borderWidth: 0.5, width: '15%', borderColor: '#ECECEC', alignItems: 'center' }}>
                        <Image
                            source={require('../image/searchLogo.png')}
                        />
                    </View>

                </View>

                <FlatList

                    data={this.state.ListAccData}

                    renderItem={({ item: item, index: index }) => {
                        return (
                            // <View><Text>{this.props.itemm.name}</Text></View>

                            // 2 thuoc tinh tu dinh nghia :
                            <ListData itemm={item} indexx={index}>

                            </ListData>
                        );
                    }
                    }
                >

                </FlatList>

                <View style={{ backgroundColor: '#004558', height: '5%', justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={styles.textbottom}>All rights reserved, CALTEX ©2017</Text>
                </View>



            </View>

        );


    }
}

const styles = StyleSheet.create({
    headercontainer: {
        backgroundColor: '#E32D44',
        left: 0,
        right: 0,
        top: 0,
        height: '16%',
        flexDirection: 'row'
    },
    textbottom: {
        fontSize: 13,
        color: '#FFFFFF'
    },
    itemName: {

        fontSize: 16,
        marginLeft: 20,
        color: '#004558',
        paddingTop: 20

    }


});