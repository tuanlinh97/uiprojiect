import React , {Component} from 'react';
import {ImageBackground,Image,View} from 'react-native';

export default class LoginSplash extends Component {
    render() {
        return(
            <ImageBackground
                source = {require('../image/bgLogin.png')}
                style = {{width:'100%',height:'100%'}}
            >
                <View style = {{justifyContent : 'center',alignItems:'center',flex:1}}>
                <Image 
                    source = {require('../image/logocaltex.png')}
                    
                />
                </View>
            </ImageBackground>
        );
    }
}